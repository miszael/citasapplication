<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\UbicacionesController;
use App\Http\Controllers\HorariosController;
use App\Http\Controllers\CitasController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('web');
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('admin/citas', [CitasController::class, 'getCitas']);
Route::post('admin/citas', [CitasController::class, 'postCita'])->name('postCita');
Route::put('admin/citas/{id}', [CitasController::class, 'putCita'])->name('putCita');
Route::delete('admin/citas/{id}', [CitasController::class, 'deleteCita'])->name('deleteCita');

Route::get('admin/ubicaciones', [UbicacionesController::class, 'getUbicaciones']);
Route::post('admin/ubicaciones', [UbicacionesController::class, 'postUbicacion'])->name('postUbicacion');
Route::put('admin/ubicaciones/{id}', [UbicacionesController::class, 'putUbicacion'])->name('putUbicacion');
Route::delete('admin/ubicaciones/{id}', [UbicacionesController::class, 'deleteUbicacion'])->name('deleteUbicacion');

Route::get('admin/horarios', [HorariosController::class, 'getHorarios']);
Route::post('admin/horarios', [HorariosController::class, 'postHorario'])->name('postHorario');
Route::put('admin/horarios/{id}', [HorariosController::class, 'putHorario'])->name('putHorario');
Route::delete('admin/horarios/{id}', [HorariosController::class, 'deleteHorario'])->name('deleteHorario');

Route::get('admin/usuarios', [UsersController::class, 'getUsers']);
Route::post('admin/usuarios', [UsersController::class, 'postUsuario'])->name('postUsuario');
Route::put('admin/usuarios/{id}', [UsersController::class, 'putUsuario'])->name('putUsuario');
Route::delete('admin/usuarios/{id}', [UsersController::class, 'deleteUsuario'])->name('deleteUsuario');


