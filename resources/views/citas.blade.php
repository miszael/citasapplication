@extends('layouts.app')
@section('content')
    <div class="row">
        <h1 class="h3 mb-3"><strong>Citas</strong></h1>
        <div class="col-12 col-lg-8 col-xxl-9 d-flex">
            <div class="card flex-fill">
                <div class="card-header">
                    <div class="d-flex justify-content-start">
                        <h5 class="card-title mb-0">Citas agendadas</h5>
                    </div>
                    <div class="d-flex justify-content-end">
                        <a class="text-success" data-bs-toggle="modal" data-bs-target="#create">
                            <div width="30" heigth="40" data-feather="plus-circle"></div>
                        </a>

                    </div>

                </div>

                <table class="table table-hover my-0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th class="d-none d-xl-table-cell">Usuario</th>
                            <th class="d-none d-xl-table-cell">Fecha</th>
                            <th class="d-none d-xl-table-cell">Calle</th>
                            <th class="d-none d-xl-table-cell">Colonia</th>
                            <th class="d-none d-xl-table-cell">Ciudad</th>
                            <th class="d-none d-xl-table-cell">Estado</th>
                            <th class="d-none d-xl-table-cell">CP</th>
                            <th class="d-none d-md-table-cell">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($citas as $cita)
                            <tr>
                                <td>{{ $cita->id }}</td>
                                <td>{{ $cita->name }}</td>
                                <td>{{ $cita->fecha }}</td>
                                <td>{{ $cita->calle }}</td>
                                <td>{{ $cita->colonia }}</td>
                                <td>{{ $cita->ciudad }}</td>
                                <td>{{ $cita->estado }}</td>
                                <td>{{ $cita->cp }}</td>
                                <td>
                                    <a href="#" class="text-warning" data-bs-toggle="modal"
                                        data-bs-target="#edit{{ $cita->id }}">
                                        <span data-feather="edit-3"></span>
                                    </a>
                                    <span> </span>
                                    <a href="#" class="text-danger" data-bs-toggle="modal"
                                        data-bs-target="#deleteCita{{ $cita->id }}">
                                        <span data-feather="trash"></span>
                                    </a>
                                </td>
                            </tr>
                            @include('modals.citasCreate')
                            @include('modals.citasEdit')
                            @include('modals.citasDelete')
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-12 col-lg-4 col-xxl-3 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">

                    <h5 class="card-title mb-0">Citas para hoy</h5>
                </div>
                <div class="card-body d-flex w-100">
                    <table class="table table-hover my-0">
                    <thead>
                        <tr>
                            <th class="d-none d-xl-table-cell">Usuario</th>
                            <th class="d-none d-xl-table-cell">Hora</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($citasNow as $citaN)
                            <tr>
                                <td>{{ $citaN->name }}</td>
                                <td>{{ $citaN->hora }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>

@endsection
