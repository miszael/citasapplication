@extends('layouts.app')
@section('content')
    <div class="row">
        <h1 class="h3 mb-3"><strong>Usuarios</strong></h1>
        <div class="col-12 col-lg-8 col-xxl-9 d-flex">
            <div class="card flex-fill">
                <div class="card-header">
                    <div class="d-flex justify-content-start">
                        <h5 class="card-title mb-0">Usuarios registrados</h5>
                    </div>
                    <div class="d-flex justify-content-end">
                        <a class="text-success" data-bs-toggle="modal" data-bs-target="#createUsuarios">
                            <div width="30" heigth="40" data-feather="plus-circle"></div>
                        </a>
                        @include('modals.usuariosCreate')
                    </div>

                </div>

                <table class="table table-hover my-0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th class="d-none d-xl-table-cell">Nombre</th>
                            <th class="d-none d-xl-table-cell">Email</th>
                            <th class="d-none d-xl-table-cell">telefono</th>
                            <th class="d-none d-md-table-cell">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($usuarios as $usuario)
                            <tr>
                                <td>{{ $usuario->id }}</td>
                                <td>{{ $usuario->name }}</td>
                                <td>{{ $usuario->email }}</td>
                                <td>{{ $usuario->telefono }}</td>
                                <td>
                                    <a href="#" class="text-warning" data-bs-toggle="modal"
                                        data-bs-target="#editUsuario{{ $usuario->id }}">
                                        <span data-feather="edit-3"></span>
                                    </a>
                                    <span> </span>
                                    <a href="#" class="text-danger" data-bs-toggle="modal"
                                        data-bs-target="#deleteUsuario{{ $usuario->id }}">
                                        <span data-feather="trash"></span>
                                    </a>
                                </td>
                            </tr>
                            @include('modals.usuariosEdit')
                            @include('modals.usuariosDelete')
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
