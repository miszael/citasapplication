@extends('layouts.app-login')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <form action="{{ route('login') }}" method="POST" class="box">
                        {{ csrf_field() }}
                        <h1>Login</h1>
                        <div class="col-lg-12{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" name="email" placeholder="Username" value="{{ old('email') }}"
                                required autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-lg-12{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" type="password" name="password" placeholder="Password" class="form-control"
                                required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label style="color: #fff">
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} ">
                                    Recuérdame
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-12 text-center">
                            <input type="submit" value="Login">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function mostrarContrasena() {
            var tipo = document.getElementById("password");
            if (tipo.type == "password") {
                tipo.type = "text";
            } else {
                tipo.type = "password";
            }
        }
    </script>
@endsection
