<div class="modal fade" id="ubicacionesDelete{{ $ubicacion->id }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Eliminar ubicacion</h2>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method="POST" action="{{ route('deleteUbicacion',  $ubicacion->id ) }}">
                @method('delete')
                {{ csrf_field() }}
                <div class="modal-body">
                    <p>
                        {{ $ubicacion->name }}
                        {{ $ubicacion->fecha }}
                        {{ $ubicacion->calle }}
                        {{ $ubicacion->colonia }}
                        {{ $ubicacion->ciudad }}
                        {{ $ubicacion->estado }}
                        {{ $ubicacion->cp }}
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" ">Eliminar</button>
                </div>
            </form>
        </div>
    </div>
</div>
