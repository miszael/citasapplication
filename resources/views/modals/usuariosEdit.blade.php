    <div class="modal fade" id="editUsuario{{ $usuario->id }}" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel">Crear nueva usuario</h2>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('putUsuario', $usuario->id) }}">
                        @method('PUT')
                        {{ csrf_field() }}

                        <div class="modal-body" id="cont_modal">

                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Nombre:</label>
                                <input type="text" name="name" class="form-control" value="{{ $usuario->name }}"
                                    required="true">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Email:</label>
                                <input type="email" name="email" class="form-control" value="{{ $usuario->email }}"
                                    required="true">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Telefono:</label>
                                <input type="number" name="telefono" class="form-control" value="{{ $usuario->telefono }}"
                                    required="true">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
