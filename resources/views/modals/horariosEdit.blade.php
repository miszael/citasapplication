    <div class="modal fade" id="horariosEdit{{ $horario->id }}" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel">Crear nuevo horario</h2>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('putHorario', $horario->id) }}">
                        @method('PUT')
                        {{ csrf_field() }}

                        <div class="modal-body" id="cont_modal">

                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Fecha:</label>
                                <input type="date" name="fecha" class="form-control" value="{{ $horario->fecha }}"
                                    required="true">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Hora:</label>
                                <input type="time" name="hora" class="form-control" value="{{ $horario->hora }}"
                                    required="true">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Ubicacion</label>
                                <select class="form-control" name="ubicacion_id">
                                    @foreach ($ubicaciones as $ubicacion)
                                        <option value="{{ $ubicacion->id }}" {{ $ubicacion->id == $horario->ubicacion_id ? 'selected' : '' }}>
                                            {{ $ubicacion->calle }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
