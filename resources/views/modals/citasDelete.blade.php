<div class="modal fade" id="deleteCita{{ $cita->id }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Eliminar cita</h2>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method="POST" action="{{ route('deleteCita', $cita->id) }}">
                @method('delete')
                {{ csrf_field() }}
                <div class="modal-body">
                    <p>
                        {{ $cita->name }}
                        {{ $cita->fecha }}
                        {{ $cita->calle }}
                        {{ $cita->colonia }}
                        {{ $cita->ciudad }}
                        {{ $cita->estado }}
                        {{ $cita->cp }}
                    </p>
                    <input type="text" hidden value="{{ $cita->id }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" ">Eliminar</button>
                </div>
            </form>
        </div>
    </div>
</div>
