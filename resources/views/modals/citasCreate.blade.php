    <div class="modal fade" id="create" tabindex="-1"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel">Crear cita</h2>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('postCita') }}">
                        {{ csrf_field() }}

                        <div class="modal-body" id="cont_modal">

                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Usuario</label>
                                <select class="form-control" name="usuario_id">
                                    <option value="starter" selected disabled >Selecciona</option>
                                    @foreach ($usuarios as $usuario)
                                        <option value="{{ $usuario->id }}" {{ $usuario->id == $cita->usuario_id }}>
                                            {{ $usuario->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Horario</label>
                                <select class="form-control" name="horario_id">
                                    <option value="starter" selected disabled >Selecciona</option>
                                    @foreach ($horarios as $horario)
                                        <option value="{{ $horario->id }}" {{ $horario->id == $cita->horario_id }}>
                                            {{ $horario->fecha }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Ubicacion</label>
                                <select class="form-control" name="ubicacion_id">
                                    <option value="starter" selected disabled >Selecciona</option>
                                    @foreach ($ubicaciones as $ubicacion)
                                        <option value="{{ $ubicacion->id }}" {{ $ubicacion->id == $cita->ubicacion_id }}>
                                            {{ $ubicacion->calle }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
