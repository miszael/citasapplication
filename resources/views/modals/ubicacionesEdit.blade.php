    <div class="modal fade" id="ubicacionesEdit{{ $ubicacion->id }}" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel">Crear nueva ubicacion</h2>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('putUbicacion', $ubicacion->id) }}">
                        @method('PUT')
                        {{ csrf_field() }}

                        <div class="modal-body" id="cont_modal">

                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Calle:</label>
                                <input type="text" name="calle" class="form-control" value="{{ $ubicacion->calle }}"
                                    required="true">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Colonia:</label>
                                <input type="text" name="colonia" class="form-control" value="{{ $ubicacion->colonia }}"
                                    required="true">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Ciudad:</label>
                                <input type="text" name="ciudad" class="form-control" value="{{ $ubicacion->ciudad }}"
                                    required="true">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Estado:</label>
                                <input type="text" name="estado" class="form-control" value="{{ $ubicacion->estado }}"
                                    required="true">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">CP:</label>
                                <input type="text" name="cp" class="form-control" value="{{ $ubicacion->cp }}"
                                    required="true">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
