<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="{{ asset('css/styles-login.css') }}" rel="stylesheet">
    {{-- <link rel="icon" href="{{ asset('dashboard/assets/images/icon.png') }}"> --}}
</head>

<body>
    <nav class="navbar top-navbar navbar-expand-md" style="background: #fff; border-bottom: 1px solid #000;">
        <div class="navbar-header" data-logobg="skin6">

            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)">
                <i class="ti-menu ti-close"></i>
            </a>
        </div>
        <div class="ml-auto p-2 bd-highlight">
            <a class="nav-link text-dark" aria-current="page" href="{{ url('/') }}">Regresar</a>
        </div>
    </nav>
    <div class="container justify-content-center">
        @yield('content')
    </div>
</body>

</html>
