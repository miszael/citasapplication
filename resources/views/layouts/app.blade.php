<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Catalogo de citas</title>
    <link rel="icon" href="{{ asset('assets/imag/icon.png') }}">
    <link href={{ asset('css/styles-dev.css') }} rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <nav id="sidebar" class="sidebar js-sidebar">
            <div class="sidebar-content js-simplebar">
                <a class="sidebar-brand" href="index.html">
                    <span class="align-middle">Citas app</span>
                </a>

                <ul class="sidebar-nav">
                    <li class="sidebar-header">
                        Pages
                    </li>
                    <li class="sidebar-item {{ (request()->is('admin/citas*')) ? 'active' : '' }}">
                        <a class="sidebar-link" href="{{ url('admin/citas') }}">
                            <i class="align-middle" data-feather="book-open"></i> <span
                                class="align-middle">Citas</span>
                        </a>
                    </li>

                    <li class="sidebar-item {{ (request()->is('admin/ubicaciones*')) ? 'active' : '' }}">
                        <a class="sidebar-link" href="{{ url('admin/ubicaciones') }}">
                            <i class="align-middle" data-feather="map"></i> <span
                                class="align-middle">Ubicaciones</span>
                        </a>
                    </li>

                    <li class="sidebar-item {{ (request()->is('admin/horarios*')) ? 'active' : '' }}">
                        <a class="sidebar-link" href="{{ url('admin/horarios') }}">
                            <i class="align-middle" data-feather="calendar"></i> <span
                                class="align-middle">Horarios</span>
                        </a>
                    </li>

                    <li class="sidebar-header">
                        Administrador
                    </li>

                    <li class="sidebar-item {{ (request()->is('admin/usuarios*')) ? 'active' : '' }}">
                        <a class="sidebar-link" href="{{ url('admin/usuarios') }}">
                            <i class="align-middle" data-feather="users"></i> <span class="align-middle">Usuarios</span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="main">
            <nav class="navbar navbar-expand navbar-light navbar-bg">
                <a class="sidebar-toggle js-sidebar-toggle">
                    <i class="hamburger align-self-center"></i>
                </a>
                <div class="navbar-collapse collapse">
                    <ul class="navbar-nav navbar-align">
                        <li class="nav-item dropdown">
                            <a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#"
                                data-bs-toggle="dropdown">
                                <i class="align-middle" data-feather="settings"></i>
                            </a>

                            <a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#"
                                data-bs-toggle="dropdown">
                                <span class="text-dark">{{ Auth::user()->name }}</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a class="dropdown-item" href="/"><i class="align-middle me-1"
                                        data-feather="layout"></i> Web</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="align-middle me-1" data-feather="log-out"></i>
                                    Log out</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>

            <main class="content">
                <div class="container-fluid p-0">
                    @yield('content')
                </div>
            </main>
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row text-muted">
                        <div class="col-6 text-start">
                            <p class="mb-0">
                                <a class="text-muted" href="#" target="_blank"><strong>Citas app</strong></a> &copy;
                            </p>
                        </div>
                        <div class="col-6 text-end">
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a class="text-muted" href="#" target="_blank">Support</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    </div>
</body>
<script src={{ asset('assets/js/scripts.js') }}></script>
<script src={{ asset('assets/js/app.js') }}></script>

</html>
