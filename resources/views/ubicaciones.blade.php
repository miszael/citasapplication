@extends('layouts.app')
@section('content')
    <div class="row">
        <h1 class="h3 mb-3"><strong>Ubicaciones</strong></h1>
        <div class="col-12 col-lg-8 col-xxl-9 d-flex">
            <div class="card flex-fill">
                <div class="card-header">
                    <div class="d-flex justify-content-start">
                        <h5 class="card-title mb-0">Ubicaciones disponibles</h5>
                    </div>
                    <div class="d-flex justify-content-end">
                        <a class="text-success" data-bs-toggle="modal" data-bs-target="#ubicacionesCreate">
                            <div width="30" heigth="40" data-feather="plus-circle"></div>
                        </a>
                        @include('modals.ubicacionesCreate')
                    </div>
                </div>
                <table class="table table-hover my-0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th class="d-none d-xl-table-cell">Calle</th>
                            <th class="d-none d-xl-table-cell">Colonia</th>
                            <th class="d-none d-xl-table-cell">Ciudad</th>
                            <th class="d-none d-xl-table-cell">Estado</th>
                            <th class="d-none d-xl-table-cell">CP</th>
                            <th class="d-none d-md-table-cell">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($ubicaciones as $ubicacion)
                            <tr>
                                <td>{{ $ubicacion->id }}</td>
                                <td>{{ $ubicacion->calle }}</td>
                                <td>{{ $ubicacion->colonia }}</td>
                                <td>{{ $ubicacion->ciudad }}</td>
                                <td>{{ $ubicacion->estado }}</td>
                                <td>{{ $ubicacion->cp }}</td>
                                <td>
                                    <a href="#" class="text-warning" data-bs-toggle="modal"
                                        data-bs-target="#ubicacionesEdit{{ $ubicacion->id }}">
                                        <span data-feather="edit-3"></span>
                                    </a>
                                    <span> </span>
                                    <a href="#" class="text-danger" data-bs-toggle="modal"
                                        data-bs-target="#ubicacionesDelete{{ $ubicacion->id }}">
                                        <span data-feather="trash"></span>
                                    </a>
                                </td>
                            </tr>
                            @include('modals.ubicacionesEdit')
                            @include('modals.ubicacionesDelete')
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
