@extends('layouts.app')
@section('content')
    <div class="row">
        <h1 class="h3 mb-3"><strong>Horarios</strong></h1>
        <div class="col-12 col-lg-8 col-xxl-9 d-flex">
            <div class="card flex-fill">
                <div class="card-header">
                    <div class="d-flex justify-content-start">
                        <h5 class="card-title mb-0">Información sobre horarios</h5>
                    </div>
                    <div class="d-flex justify-content-end">
                        <a class="text-success" data-bs-toggle="modal" data-bs-target="#horariosCreate">
                            <div width="30" heigth="40" data-feather="plus-circle"></div>
                        </a>

                    </div>

                </div>

                <table class="table table-hover my-0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th class="d-none d-xl-table-cell">Fecha</th>
                            <th class="d-none d-xl-table-cell">Hora</th>
                            <th class="d-none d-xl-table-cell">Calle</th>
                            <th class="d-none d-xl-table-cell">Colonia</th>
                            <th class="d-none d-xl-table-cell">Ciudad</th>
                            <th class="d-none d-xl-table-cell">Estado</th>
                            <th class="d-none d-xl-table-cell">CP</th>
                            <th class="d-none d-md-table-cell">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($horarios as $horario)
                            <tr>
                                <td>{{ $horario->id }}</td>
                                <td>{{ $horario->fecha }}</td>
                                <td>{{ $horario->hora }}</td>
                                <td>{{ $horario->calle }}</td>
                                <td>{{ $horario->colonia }}</td>
                                <td>{{ $horario->ciudad }}</td>
                                <td>{{ $horario->estado }}</td>
                                <td>{{ $horario->cp }}</td>
                                <td>
                                    <a href="#" class="text-warning" data-bs-toggle="modal"
                                        data-bs-target="#horariosEdit{{ $horario->id }}">
                                        <span data-feather="edit-3"></span>
                                    </a>
                                    <span> </span>
                                    <a href="#" class="text-danger" data-bs-toggle="modal"
                                        data-bs-target="#horariosDelete{{ $horario->id }}">
                                        <span data-feather="trash"></span>
                                    </a>
                                </td>
                            </tr>
                            @include('modals.horariosEdit')
                            @include('modals.horariosDelete')
                            @include('modals.horariosCreate')
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
