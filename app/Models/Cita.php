<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    use HasFactory;

    /**
     * The database connection
     *
     * @
     * */

    protected $connection = 'mysql';

    /**
     * El nombre de la tabla donde se almacena los datos
     * @var String
     * @access protected
     */

    protected $table = 'Citas';
}
