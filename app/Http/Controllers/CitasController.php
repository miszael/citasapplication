<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cita;
use App\Models\Ubicacion;
use App\Models\Horario;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class CitasController extends Controller
{
    public function getCitas()
    {
        $citas = DB::connection('mysql')->select(
            "SELECT c.id as id,
             us.name,
             c.usuario_id,
             c.horario_id,
             c.ubicacion_id,
             h.fecha,
             u.calle ,
             u.colonia ,
             u.ciudad,
             u.estado ,
             u.cp
             FROM
                Citas c
            INNER JOIN users us on
                us.id = c.usuario_id
            INNER JOIN Horarios h on
                h.id = c.horario_id
            INNER JOIN Ubicaciones u on
                u.id = c.ubicacion_id
            ;"
        );
        $citasNow = DB::connection('mysql')->select(
            "SELECT c.id as id,
             us.name,
             c.usuario_id,
             c.horario_id,
             c.ubicacion_id,
             h.fecha,
             u.calle ,
             u.colonia ,
             u.ciudad,
             u.estado ,
             u.cp
             FROM
                Citas c
            INNER JOIN users us on
                us.id = c.usuario_id
            INNER JOIN Horarios h on
                h.id = c.horario_id
            INNER JOIN Ubicaciones u on
                u.id = c.ubicacion_id
            WHERE DATE_FORMAT(h.fecha,'%Y-%m-%d')='CURDATE()';"
        );

        $ubicaciones = Ubicacion::select('id','calle','colonia','ciudad','estado')->get();
        $horarios = Horario::select('id','fecha','hora')->get();
        $usuarios = User::select('id','name')->get();

        return view('citas', [
            'citas' => $citas,
            'citasNow' => $citasNow,
            'ubicaciones' => $ubicaciones,
            'horarios' => $horarios,
            'usuarios' => $usuarios
        ]);
    }

    public function postCita(Request $req)
    {
        $cita = new Cita;
        $cita->usuario_id = $req['usuario_id'];
        $cita->horario_id = $req['horario_id'];
        $cita->ubicacion_id = $req['ubicacion_id'];
        $cita->save();

        return back();
    }

    public function putCita(Request $req, $id)
    {
        Cita::where('id','=', $id)
        ->update([
            "usuario_id" => $req['usuario_id'],
            "horario_id" => $req['horario_id'],
            "ubicacion_id" => $req['ubicacion_id']
        ]);
        return back();
    }

    public function deleteCita($id)
    {
        Cita::where('id','=', $id)->delete();
        return back();
    }
}
