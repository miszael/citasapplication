<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ubicacion;
use Illuminate\Support\Facades\DB;
class UbicacionesController extends Controller
{
    public function getUbicaciones()
    {
        $colums = array('*');
       $ubicaciones = Ubicacion::select($colums)->get();

       return view('ubicaciones', ['ubicaciones' => $ubicaciones]);
    }

    public function postUbicacion(Request $req)
    {
        $ubicacion = new Ubicacion;
        $ubicacion->calle = $req['calle'];
        $ubicacion->colonia = $req['colonia'];
        $ubicacion->ciudad = $req['ciudad'];
        $ubicacion->estado = $req['estado'];
        $ubicacion->cp = $req['cp'];
        $ubicacion->save();
        return back();
    }

    public function putUbicacion(Request $req, $id)
    {
        Ubicacion::where('id', '=', $id)
        ->update([
            'calle' => $req['calle'],
            'colonia' => $req['colonia'],
            'ciudad' => $req['ciudad'],
            'estado' => $req['estado'],
            'cp' => $req['cp']
        ]);
        return back();
    }

    public function deleteUbicacion($id)
    {
        Ubicacion::where('id', '=', $id)->delete();
        return back();
    }

}
