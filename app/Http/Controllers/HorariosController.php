<?php

namespace App\Http\Controllers;

use App\Models\Horario;
use Illuminate\Support\Facades\DB;
use App\Models\Ubicacion;
use Illuminate\Http\Request;

class HorariosController extends Controller
{
    public function getHorarios()
    {
        $horarios = DB::connection('mysql')->select(
            "SELECT h.id as id,
            h.fecha,
            h.hora,
            h.ubicacion_id,
            u.calle ,
            u.colonia ,
            u.ciudad,
            u.estado ,
            u.cp
            FROM
            horarios h
            INNER JOIN ubicaciones u ON
            u.id=h.ubicacion_id;");

            $ubicaciones = Ubicacion::select('id','calle','colonia','ciudad','estado')->get();

            return view('horarios',[
                'horarios' => $horarios,
                'ubicaciones' => $ubicaciones
            ]);
    }

    public function postHorario(Request $req)
    {
        $horarios = new Horario();
        $horarios->fecha = $req['fecha'];
        $horarios->hora = $req['hora'];
        $horarios->ubicacion_id = $req['ubicacion_id'];
        $horarios->save();

        return back();
    }

    public function putHorario(Request $req, $id)
    {
        Horario::where('id', '=', $id)
        ->update([
            'fecha' => $req['fecha'],
            'hora' => $req['hora'],
            'ubicacion_id' => $req['ubicacion_id']
        ]);

        return back();
    }

    public function deleteHorario($id)
    {
        Horario::where([['id', '=', $id],])->delete();
        return back();
    }
}
