<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function getUsers()
    {
        $users = User::select('id','name','email','telefono')->get();
        return view('usuarios', ['usuarios' => $users]);
    }

    public function postUsuario(Request $req)
    {
        $ubicacion = new User;
        $ubicacion->name = $req['name'];
        $ubicacion->email = $req['email'];
        $ubicacion->telefono = $req['telefono'];
        $ubicacion->password = Hash::make($req['password']);
        $ubicacion->save();
        return back();
    }

    public function putUsuario(Request $req, $id)
    {
        User::where('id', '=', $id)
        ->update([
            'name' => $req['name'],
            'email' => $req['email'],
            'telefono' => $req['telefono']
        ]);
        return back();
    }

    public function deleteUsuario($id)
    {
        User::where('id', '=', $id)->delete();
        return back();
    }
}
