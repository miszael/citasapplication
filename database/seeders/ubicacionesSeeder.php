<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ubicacionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = new \DateTime();
        DB::table('ubicaciones')->insert([
            'calle'=>'Rio Nazas',
            'colonia' => 'San Jose',
            'ciudad' => 'Tequixquiac',
            'estado' => 'Estado de México',
            'cp' => '55654',
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }
}
