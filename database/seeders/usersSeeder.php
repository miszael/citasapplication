<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class usersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = new \DateTime();
        DB::table('users')->insert([
            'name'=>'Administrador',
            'email' => 'misaelvgm011@gmail.com',
            'password' => Hash::make('misaelvgm011'),
            'telefono' => '5537215343',
            'email_verified_at' => $now,
            'created_at' => $now,
            'updated_at' => $now
        ]);
        DB::table('users')->insert([
            'name'=>'Misael',
            'email' => 'Misael@gmail.com',
            'password' => Hash::make('password'),
            'telefono' => '5537215348',
            'email_verified_at' => $now,
            'created_at' => $now,
            'updated_at' => $now
        ]);
        DB::table('users')->insert([
            'name'=>'Randy',
            'email' => 'Randy@gmail.com',
            'password' => Hash::make('password'),
            'telefono' => '5537215345',
            'email_verified_at' => $now,
            'created_at' => $now,
            'updated_at' => $now
        ]);
        DB::table('users')->insert([
            'name'=>'Joss',
            'email' => 'Joss@gmail.com',
            'password' => Hash::make('password'),
            'telefono' => '5537215346',
            'email_verified_at' => $now,
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }
}
