<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class horariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = new \DateTime();
        DB::table('horarios')->insert([
            'fecha'=>'2021-08-06',
            'hora' => '23:07',
            'ubicacion_id' => '1',
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }
}
