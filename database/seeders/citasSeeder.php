<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class citasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $now = new \DateTime();
        DB::table('citas')->insert([
            'usuario_id'=>'1',
            'horario_id' => '1',
            'ubicacion_id' => '1',
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }
}
